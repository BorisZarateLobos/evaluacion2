<%-- 
    Document   : lista
    Created on : 24-04-2021, 15:47:48
    Author     : Admin
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="com.mycompany.evaluacion2.Personas"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Personas> lista = (List<Personas>) request.getAttribute("listaClientes");
    Iterator<Personas> itlista = lista.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
         <form  name="form" action="BaseController" method="POST"> 
        <table border="1">
                    <thead>
                    <th>Rut</th>
                    <th>nombre </th>
                    <th>telefono </th>
                    <th>direccion </th>
                    <th>email </th>
                    
                    <th> </th>
         
                    </thead>
                    <tbody>
                        <%while (itlista.hasNext()) {
                       Personas cm = itlista.next();%>
                        <tr>
                            <td><%= cm.getRut()%></td>
                            <td><%= cm.getNombre()%></td>
                            <td><%= cm.getTelefono()%></td>
                            <td><%= cm.getDireccion()%></td>
                            <td><%= cm.getEmail()%></td> 
                     <td> <input type="radio" name="seleccion" value="<%= cm.getRut()%>"> </td>
                     
                
                        </tr>
                        <%}%>                
                    </tbody>           
                </table>
         </form>
            <button type="submit" name="accion" value="eliminar" class="btn btn-success">Eliminar cliente</button>
            <button type="submit" name="accion" value="editar" class="btn btn-success">Editar</button>
    </body>
</html>
