/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.evaluacion2;

import java.util.ArrayList;
import cl.controller.dao.PersonasJpaController;
import cl.controller.dao.exceptions.NonexistentEntityException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Admin
 */
@Path("ingresos")
public class Ingreso {
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarIngresos(){
        PersonasJpaController dao=new PersonasJpaController();
        List<Personas> lista= dao.findPersonasEntities();
        return Response.ok(200).entity(lista).build();
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response Add(Personas personas) {
       
    return Response.ok(200).entity(personas).build();
    }
    
    @DELETE
    @Path("/{iddelete}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("iddelete") String iddelete) {
    
        try {
            PersonasJpaController dao=new PersonasJpaController();
            dao.destroy(iddelete);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(Ingreso.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok("cliente eliminado").build();
    }
    
    @PUT
    public Response update(Personas personas){
        try {
            PersonasJpaController dao=new PersonasJpaController();
            dao.edit(personas);
        } catch (Exception ex) {
            Logger.getLogger(Ingreso.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(200).entity(personas).build();
    }
    
}
